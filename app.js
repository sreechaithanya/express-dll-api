const express = require('express');
const edge = require('edge-js');
const path = require('path');

const app = express();
const port = 3000; 

const assemblyPath = path.join('C:\\Users\\Administrator\\Documents\\dotnet', 'Sample.dll');

const sample = edge.func(`
    #r "${assemblyPath}"

    using SampleLibrary;
    async (input) => {
        var sample = new SampleClass();
        return sample.SampleFunction();
    }
`);

app.get('/api/sample', (req, res) => {
    sample(null, function (error, result) {
        if (error) {
            console.error(error);
            return res.status(500).send('Error executing the logic.');
        }
        console.log(result);
        res.send(result);
    });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
